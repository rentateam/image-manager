<?php
namespace Astartsky\ImageManager\Silex;

use Astartsky\ImageManager\ImageHelper;
use Astartsky\ImageManager\ImageManager;
use Silex\Application;
use Silex\ServiceProviderInterface;

class ImageManagerServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     * @param Application $app An Application instance
     */
    public function register(Application $app)
    {
        $app['image_manager'] = function() use ($app) {
            $imageManager = new ImageManager();
            $imageManager->setStorage($app['image_manager_storage']);

            return $imageManager;
        };

        $app['image_helper'] = function() use ($app) {
            return new ImageHelper($app['image_manager'], isset($app['image_manager_regenerate_images']) ? $app['image_manager_regenerate_images'] : false);
        };

        if (class_exists("Twig_Environment")) {

            $app['twig'] = $app->extend('twig', function($twig) use ($app) {
                /** @var $twig \Twig_Environment */

                try {

                $twig->addFilter(new \Twig_SimpleFilter('image', function ($file, $name) use ($app) {
                    try {
                        $url = $app['image_helper']->getUrl($name, $file);
                    } catch (\Exception $e) {
                        $url = '';
                    }

                    return $url;

                }));

                } catch (\LogicException $e) {}

                return $twig;
            });


        }
    }

    /**
     * Bootstraps the application.
     */
    public function boot(Application $app)
    {

    }
}
