<?php
namespace Astartsky\ImageManager;

use Astartsky\ImageManager\ImageDefinition\ImageDefinitionInterface;

class ImageHelper
{
    /**
     * @var ImageManager
     */
    protected $im;

    /**
     * @var bool
     */
    protected $regenerate;

    /**
     * @param ImageManager $im
     * @param bool $regenerate
     */
    public function __construct(ImageManager $im, $regenerate = false)
    {
        $this->im = $im;
        $this->regenerate = $regenerate;
    }

    /**
     * @param string $name
     * @param string $file
     * @return null|string
     * @throws \Exception
     */
    public function getUrl($name, $file)
    {
        /** @var ImageDefinitionInterface $image */
        $image = $this->im->get($name);
        if (!$image instanceof ImageDefinitionInterface) {
            throw new \Exception("No {$name} image definition registered");
        }

        if ($this->regenerate || false === is_file($image->getPath($file))) {
            $destination = $image->convert($file);
            $file = basename($destination);
        }

        return $image->getUrl($file);
    }

    /**
     * @param string $name
     * @param string $file
     * @param string|null $title
     * @param string|null $alt     
     * @return string
     */
    public function getImgTag($name, $file, $title = null, $alt = null)
    {
        return '<img src="' . $this->getUrl($name, $file) . '" '.($title ? 'title="'.htmlspecialchars($title).'"' : '').' '.($alt ? 'alt="'.htmlspecialchars($alt).'"' : '').' />';
    }
} 