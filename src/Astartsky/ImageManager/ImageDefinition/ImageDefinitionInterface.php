<?php
namespace Astartsky\ImageManager\ImageDefinition;

interface ImageDefinitionInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $file
     * @return string
     */
    public function getPath($file);

    /**
     * @param string $file
     * @return string
     */
    public function getUrl($file);

    /**
     * @param string $file
     * @return string
     */
    public function convert($file);
}