<?php
namespace Astartsky\ImageManager\ImageDefinition;

use Astartsky\ImageManager\ImageStorage\ImageStorageInterface;

abstract class AbstractImageDefinition implements ImageDefinitionInterface
{
    /** @var ImageStorageInterface */
    protected $storage;

    /**
     * @param ImageStorageInterface $storage
     */
    public function setStorage(ImageStorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param string $file
     * @return string
     */
    public function getPath($file)
    {
        return $this->storage->getPath($this, $file);
    }

    /**
     * @param string $file
     * @return string
     */
    public function getUrl($file)
    {
        return $this->storage->getUrl($this, $file);
    }

    /**
     * @return string
     */
    abstract public function getName();

    /**
     * @param string $file
     * @return string
     */
    abstract public function convert($file);
}